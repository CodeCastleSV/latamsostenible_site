<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'latamsostenible' );

/** MySQL database username */
define( 'DB_USER', 'phpmyadmin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'D14nA%2019' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'C8IV}zq]fHeYjK3.fZLx]Jb{n^SW(/luhJo{?T/cF(d6WG--kVD~|vpDZp5$8~+O' );
define( 'SECURE_AUTH_KEY',  '[cUx5yF:UC70m3n2*uT3b~?(4uc*90|5/XUrL;9g4ra{cp0=)JpdrohS(J-aMpXr' );
define( 'LOGGED_IN_KEY',    'dIn)3kxqm-92J(k5onRVDa1mt5xvg@QZg$XKb!H5W6:fj{:Pc(7Wx5j`BY[(}}}{' );
define( 'NONCE_KEY',        'z<FNvIPRc r|LaS0En5cf]>yRwTf &aEM?;`R!}X[[~SZ>2&bS>{FeZP;j|9s?@U' );
define( 'AUTH_SALT',        'ks9(ZGmfVv,~:`V)Rp~65rH@<4/sQCxzsWN_i!s3?w|hO/FMc(t#Lpwk7.+RD[rC' );
define( 'SECURE_AUTH_SALT', 'gL,~oEOKy]Lo;vwuSNaYE&E9,6fAh)~G)g)rn=6:87bc86@OXO^f9?T2I;I*.)B9' );
define( 'LOGGED_IN_SALT',   'm,>58$k%lbXWTKD<AqGP,~LI^E5psk<V,^aK}2%w$^WcQlTAQ+AIv#fO6$R-mBI[' );
define( 'NONCE_SALT',       'Pa&!]:HH<V/)LmHPGh*jv-;54;3 ^Co=hOJzW`AKmU2:%VFgh&6Se1P5.II{_6fy' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
