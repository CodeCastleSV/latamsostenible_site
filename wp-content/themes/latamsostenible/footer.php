<div class="container-fluid footer-menu" style="background-color: #1c2b31; padding: 2rem 4rem;">
	<div class="site-menu">
		<div class="row">
			<div class="col-12 col-md-4 brand-footer">
				<a class="navbar-brand-footer" href="<?php echo esc_url( home_url( '/' ) ); ?>">
        	<?php
			    	// Mostrar logo
			   			$custom_logo_id = get_theme_mod( 'custom_logo' );
			   			$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

			    	if ($image == ""){
			    		echo "<h1>".bloginfo('name')."</h1>";
			    	} else {
			    		// Logo
			    		echo "<img src='".$image[0]."' alt='logo' class='img-fluid'>";
			    	}
			    ?>
        </a>
        <br><br>
        <p><?php echo bloginfo('description'); ?></p>	    
			</div>
			<div class="col-12 col-md-4 redes-sociales-footer tex.center">
				<br><br>
				<p>SIGUENOS POR</p>
				<?php
		   		function theme_social_menu() {
				    if (has_nav_menu('social')) {
			        wp_nav_menu(
		            array(
		              'theme_location' => 'social',
		              'container' => 'div',
		              'container_class' => 'theme-social-menu',
		              'depth' => 1, // No need to allow sub-menu for social icons
		              'menu_class' => 'menu-social',
		              'fallback_cb' => false, // No fallback setting
		              'link_before' => '<span class="screen-reader-text">', // Hide the social links text
		              'link_after' => '</span>', // Hide the social links text
		              'items_wrap' => '<ul class="%2$s">%3$s</ul>',
		            )
			        );
				    }
					}
		   	?>
				<?php if (has_nav_menu('social')) : ?>
		    <div class="social-menu">
		      <?php theme_social_menu(); ?>
		    	</div>
				<?php endif; ?>
				<br><br>
			</div>
			<div class="col-12 col-md-4">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'footer',
						'menu_id' => 'footer-menu',
					) );
				?>
			</div>
		</div>
	</div>
</div>
<footer>
   <p class="text-center"><b>Todos los derechos reservados <?php echo date("Y"); ?></b></p>
</footer>
    <?php wp_footer(); ?>

    <?php

    	//URLs
    	$home_url = home_url();
    	$url = $home_url."/wp-content/themes/latamsostenible/main.js";

    	$fb_url = $home_url."/wp-content/themes/latamsostenible/assets/icon-facebook.png";
    	$lk_url = $home_url."/wp-content/themes/latamsostenible/assets/icon-linkedin.png";
    	$tw_url = $home_url."/wp-content/themes/latamsostenible/assets/icon-twitter.png";
    	$especialist_url = $home_url."/wp-content/themes/latamsostenible/assets/logo-esp.png";

    ?>
    <!-- javascripts links -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo $url; ?>"></script>

		<script type="text/javascript">
			$(document).ready(function(){

				//Menú footer redes sociales
				$(".redes-sociales-footer *").html(function(buscayreemplazalk, reemplazalk) {
			     return reemplazalk.replace('Linkedin', '<img src="<?php echo $lk_url; ?>" class="img-fluid social-icons">');
			  });

			  $(".redes-sociales-footer *").html(function(buscayreemplazafb, reemplazafb) {
			     return reemplazafb.replace('Facebook', '<img src="<?php echo $fb_url; ?>" class="img-fluid social-icons">');
			  });

			  $(".redes-sociales-footer *").html(function(buscayreemplazatw, reemplazatw) {
			     return reemplazatw.replace('Twitter', '<img src="<?php echo $tw_url; ?>" class="img-fluid social-icons">');
			  });

			  $(".es-field-wrap *").html(function(buscayreemplaza, reemplaza) {
			     return reemplaza.replace('Email*', '');
			  });

			  $(".es_txt_email ").attr("placeholder","Escribe tu correo electrónico");

			  //Hover Especialidades
			  $(".eihe-caption").addClass("text-center");
			  $(".eihe-caption").append('<img src="<?php echo $especialist_url; ?>" class="img-fluid text-center logo-esp">');

			  //Columnas de especialidades
			  $("#specialist-container").children("div").attr("id","specialist-container-default");
			  $("#specialist-container-default").children("div").attr("id","specialist-row-default");
			  $("#specialist-row-default").children("div:nth-child(2)").addClass("specialist-item-column");
			  $("#specialist-row-default").children("div:nth-child(3)").addClass("specialist-item-column");
			  $("#specialist-row-default").children("div:nth-child(4)").addClass("specialist-item-column");
			  $("#specialist-row-default").children("div:nth-child(5)").addClass("specialist-item-column");

			  // Post
			  $("#new-post").find("h2").addClass("titulo");
			  $("#posts-container").find("article").addClass("post-item");
			  $(".post-item").find("img").addClass("post-img");

			});
		</script>
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            facebook: "713874832044848", // Facebook page ID
            whatsapp: "50376335878", // WhatsApp number
            call_to_action: "¿Te ayudamos?", // Call to action
            button_color: "#A8CE50", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,whatsapp", // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->

  </body>
</html>