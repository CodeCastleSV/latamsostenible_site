<!DOCTYPE html>
<html <?php language_attributes(); ?>
 <head>
   <title><?php bloginfo('name'); ?> &raquo; <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
   <meta charset="<?php bloginfo( 'charset' ); ?>">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <!-- stylesheets links -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

   <!-- theme main stylesheet --> 
   <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
   <?php wp_head(); ?>

 </head>
 <body <?php body_class(); ?>>
   <header class="">
    <nav class="navbar navbar-expand-lg">
      <div class="container">
        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
        	<?php
			    	// Mostrar logo
			   			$custom_logo_id = get_theme_mod( 'custom_logo' );
			   			$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

			    	if ($image == ""){
			    		echo "<h1>".bloginfo('name')."</h1>";
			    	} else {
			    		// Logo
			    		echo "<img src='".$image[0]."' alt='logo' class='img-fluid'>";
			    	}
			    ?>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu" aria-controls="mainMenu" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="mainMenu">
          <div class="mr-auto"></div>
          <?php
					 wp_nav_menu(array( 
					 	'theme_location' => 'header',
					 	'menu_id' => 'menu-principal',
					 	'menu_class' => 'navbar-nav form-inline my-2 my-md-0',
					 	'header-menu' => 'header-menu' )); 
					?>
        </div>
      </div>
    </nav>
 </header>
 