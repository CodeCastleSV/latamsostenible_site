<?php
// This function enqueues the Normalize.css for use. The first parameter is a name for the stylesheet, the second is the URL. Here we
// use an online version of the css file.
function add_normalize_CSS() {
    wp_enqueue_style( 'normalize-styles', "https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css");
}

// Register a new sidebar simply named 'sidebar'
function add_widget_Support() {
                register_sidebar( array(
                                'name'          => 'Sidebar',
                                'id'            => 'sidebar',
                                'before_widget' => '<div>',
                                'after_widget'  => '</div>',
                                'before_title'  => '<h2>',
                                'after_title'   => '</h2>',
                ) );
}
// Hook the widget initiation and run our function
add_action( 'widgets_init', 'add_Widget_Support' );

// Función para obtener logo desde el admin
add_theme_support( 'custom-logo', array(
	'height'      => 100,
	'width'       => 400,
	'flex-height' => true,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
) );

function theme_setup() {
  register_nav_menus(array(
  	'header' => esc_html__('Header Menu', 'Header Menu'),
    'social' => esc_html__('Social Menu', 'Social Menu'),
    'footer' => esc_html__( 'Footer Menu', 'Footer Menu' )
  ));
}
add_action('after_setup_theme', 'theme_setup');

// Imagen destacada de post
if ( function_exists( 'add_theme_support' ) )
add_theme_support( 'post-thumbnails' );


